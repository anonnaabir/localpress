<main id="main" class="site-main" role="main">

	<header class="page-header">
		<h1 class="entry-title"><?php _e( 'The page can&rsquo;t be found.', 'localpress' ); ?></h1>
	</header>

	<div class="page-content">
		<p><?php _e( 'It looks like nothing was found at this location.', 'localpress' ); ?></p>
	</div>

</main>
