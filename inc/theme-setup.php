<?php
				
            function localpress_init() {

                require get_template_directory() . '/inc/plugin-setup.php';

                add_theme_support('custom-logo');
                add_theme_support( 'title-tag' );
                add_theme_support( 'post-thumbnails' );
                add_theme_support( 'automatic-feed-links' );
                add_theme_support( 'title-tag' );
                add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
                add_theme_support( 'custom-logo', array(
                    'width' => 260,
                    'height' => 100,
                    'flex-height' => true,
                    'flex-width' => true,
                ) );
                add_theme_support( 'custom-header' );
                add_theme_support( 'woocommerce' );
                add_post_type_support( 'page', 'excerpt' );

                register_nav_menus(
                    array( 'main-menu' => __( 'Main Menu', 'localpress' ) )
                );

                load_theme_textdomain( 'localpress', get_template_directory() . '/languages' );
            }


            add_action( 'after_setup_theme', 'localpress_init' );


            function localpress_comment_reply() {
                if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
            }
            add_action( 'comment_form_before', 'localpress_comment_reply' );
