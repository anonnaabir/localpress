<?php

    	// Enqueue scripts and styles.

		function localpress_scripts() {
			$theme = wp_get_theme();
			$scripts_version = $theme->get('Version');
			
			wp_enqueue_style('localpress_main_style', get_template_directory_uri() . '/css/theme.min.css', array(), $scripts_version);
			
			}
            
			add_action( 'wp_enqueue_scripts', 'localpress_scripts', 100);