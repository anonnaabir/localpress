<?php


			// Demo Import Function Start

			function localpress_demo_import() {
				return array(
					array(
						'import_file_name'           => 'Breakfast Restaurant',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://preview.themespell.com/wp-content/uploads/2021/04/breakfastrestaurant.WordPress.2021-04-14.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/04/localpress_break-fast_demo.png',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'localpress' ),
						'preview_url'                => 'https://preview.themespell.com/localpress-breakfast-restaurant/',
					),
					array(
						'import_file_name'           => 'Plumber Service',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://preview.themespell.com/wp-content/uploads/2021/04/localpressplumboy.WordPress.2021-04-14.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/04/localpress_plumber_demo.png',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'localpress' ),
						'preview_url'                => 'https://preview.themespell.com/localpress-plumboy/',
					),
					array(
						'import_file_name'           => 'Medical Service',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://preview.themespell.com/wp-content/uploads/2021/04/medicalservice.WordPress.2021-04-14.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/04/localpress_medica_demo.png',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'localpress' ),
						'preview_url'                => 'https://preview.themespell.com/localpress-medical-service/',
					),
					array(
						'import_file_name'           => 'Auto Repair Service',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://preview.themespell.com/wp-content/uploads/2021/04/localpressautorepairservice.WordPress.2021-04-14.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/04/localpress_auto-repair_demo.png',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'localpress' ),
						'preview_url'                => 'https://preview.themespell.com/localpress-auto-repair-service/',
					),
					
					array(
						'import_file_name'           => 'Non-profit Service',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://preview.themespell.com/wp-content/uploads/2021/04/let039sdo.WordPress.2021-04-14.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/04/localpress_lets_do_demo.png',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'localpress' ),
						'preview_url'                => 'https://preview.themespell.com/localpress-lets-do/',
					),
				);
			}
			add_filter( 'pt-ocdi/import_files', 'localpress_demo_import' );
		
		
		
		
			function localpress_demo_import_page_setup( $default_settings ) {
				$default_settings['parent_slug'] = 'themes.php';
				$default_settings['page_title']  = esc_html__( 'LocalPress Demo Import' , 'localpress' );
				$default_settings['menu_title']  = esc_html__( 'Import LocalPress Demos' , 'localpress' );
				$default_settings['capability']  = 'import';
				// $default_settings['menu_slug']   = 'pt-one-click-demo-import';
			
				return $default_settings;
			}
			add_filter( 'pt-ocdi/plugin_page_setup', 'localpress_demo_import_page_setup' );
		
		
			function localpress_demo_import_page_title ($plugin_title ) {
				?>
				<h1 class="ocdi__title  dashicons-before  dashicons-upload"><?php $plugin_title  = esc_html_e( 'LocalPress Demo Import', 'localpress' ); ?></h1>
				<?php
			}
			add_filter( 'pt-ocdi/plugin_page_title', 'localpress_demo_import_page_title' );
		
			add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

			add_filter( 'pt-ocdi/import_memory_limit', '256M' );
		
		
		
			
		
			// Demo Import Function End






