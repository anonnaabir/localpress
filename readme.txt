=== LocalPress ===

Requires at least: 4.5
Tested up to: 5.7
Requires PHP: 7.0
Stable tag: 1.0
License: GNU General Public License v2 or later
License URI: LICENSE

The Best Local Business WordPress Theme That Ever Been Made

== Description ==

No matter if you are running a small local startup, business, or an enterprise, you will always be able to build the best website for your local business using the LocalPress local business WordPress theme. Quality comes with high price, therefore choose the best one. LocalPress theme is entirely focused on Local Business. You handle the business and leave your website to LocalPress local business WordPress theme.



== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.



== Changelog ==

= 1.0 - October 04, 2021 =
* Initial release