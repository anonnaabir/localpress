<?php
        if ( !defined( 'ABSPATH' ) ) { exit; }
        if ( ! isset( $content_width ) ) $content_width = 1280;

        if ( ! defined( 'LOCALPRESS_VERSION' ) ) {
            define( 'LOCALPRESS_VERSION', '1.0' );
        }

        // Required Functions Start

        require get_template_directory() . '/inc/enqueue.php';   // Script Enqueue Functions
        require get_template_directory() . '/inc/theme-setup.php';  // All Theme Setup Functions
        require get_template_directory() . '/inc/template-functions.php';


        // Load Jetpack compatibility file.

        if ( defined( 'JETPACK__VERSION' ) ) {
            require get_template_directory() . '/inc/jetpack.php';
        }


        function localpress_register_elementor_locations( $elementor_theme_manager ) {
            $elementor_theme_manager->register_all_core_location();
        };
        add_action( 'elementor/theme/register_locations', 'localpress_register_elementor_locations' );


